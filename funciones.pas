program funciones (input,output);

uses crt;
var 
r1,rr,a,c,opcion,val1,val2:integer;
r2,r4,valor:real;

Procedure f_math;

begin
	Writeln('  1. Funcion Matematica ');
	writeln;writeln;
	Writeln('  Funciones Racionales  ');
	writeln;
	Writeln('Para determinar los puntos en la grafica de la siguiente Funcion racional:');
	Writeln;
	Writeln('     f (x) = 3x + 5x^2 / 2x^2 -1     ');
	writeln('Determinar el valor de Y cuando X valga:');
	writeln;
			Readln(a);
			rr:=sqr(a);
			r1:=(2*rr)-(1);
			r2:=((3*a)+(5*rr))/r1;
			Writeln('El Valor de Y cuando X vale: ', a ,' es: ', r2:2:2)
			writeln('Donde su DOMINIO son todos los numeros REALES con excepcion del 1/2');
			writeln('Y su RANGO son todos los numeros reales con excepcion del 3/2');
			Writeln; 
			readkey;
			clrscr;
end;

Procedure f_tri;
begin
 writeln('  2.funcion trigonometrica ');
    writeln;
    writeln('  funcion tangente         ');
    writeln(' Para determinar el valor de la tangente');
    writeln(' hay que sacar el valor de seno y coseno');
    writeln(' y dividirlos dado en la formula tag:sin(a)/cos(b)');
    writeln;
    writeln('ingresar valor de seno');
    readln(val1);
    writeln('ingresar valor de coseno');
    readln(val2);
    valor:=sin(val1)/cos(val2);
    writeln('valor de la tangente es:',valor:2:2);
    readkey;
    clrscr;

end; 

Procedure f_ord;
begin
  writeln('  3.funciones ordinarias ');
  writeln;
  writeln('  funcion de diferencial paralela');
  writeln('  ingresar primer valor ');
  readln(val1);
  writeln('  ingresar segundo valor ');
  readln(val2);
  valor:=(val1+val2)+(val1+val2);
  writeln('valor es igual a:',valor:2:2);
  readkey;
  clrscr;
end; 

Procedure f_esp;

begin
	writeln('4. Funcion Especial');Writeln;
	writeln('En una funcion: ');
	writeln('     e^y = x ');
	writeln('Determinar el valor de y');
	writeln('Introduzca el valor de x: ');
	readln(c);
	r4:=ln(c);
	writeln('El valor de y es: ', round(r4));
		readkey;
		clrscr;
end; 

BEGIN
			clrscr;
			repeat
	Writeln;
	Writeln('          Bienvenido            ');
	Writeln;
	Writeln('    1. Funcion Matematica ');
	Writeln('    2. Funcion Trigonometrica   ');
	Writeln('    3. Funcion Ordinaria ');
	Writeln('    4. Funcion Especial ');
	Writeln('    5. Salir ');
	Writeln('********************************');
	Readln(opcion);
			clrscr;
			begin
		case opcion of
	
			1:  Begin
					f_math;
				End;
       
			2: Begin
					f_tri;
			   End;
       
			3: Begin
					f_ord;
			   End;
       
			4: Begin
					f_esp;
			   End;
			   
			5: Writeln('SALIR');
		end; 
		end;
			Until(opcion=5);
				Readkey;
			
END.
